//
//  task_managerApp.swift
//  task_manager
//
//  Created by Rodrigo Sarmento on 25/05/23.
//

import SwiftUI

@main
struct task_managerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
            BottomView()
        }
    }
}
