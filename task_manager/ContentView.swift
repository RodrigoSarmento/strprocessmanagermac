//
//  ContentView.swift
//  task_manager
//
//  Created by Rodrigo Sarmento on 25/05/23.
//

import SwiftUI
import Foundation
import Combine

struct DataEntry: Identifiable, Hashable {
    let id = UUID()
    let pid: String
    let threads: String
    let command: String
    let cpu: String
    let time: String
    let memory: String
    let state:String
    let user:String
    let priority:String
}
var filterText = ""


class PSCommandDataProvider: ObservableObject {
    private var cancellable: AnyCancellable?
    @Published var exampleArray: [DataEntry] = []

    func fetchData() {
        let task = Process()
        task.launchPath = "/bin/bash"
        task.arguments = ["-c", "while true; do ps Ao pid,comm,%cpu,wq,time,%mem,state,user,ni -r | head -n 100 | tail -n +2; sleep 3;done"]

        let outputPipe = Pipe()
        task.standardOutput = outputPipe
        
        let outputHandle = outputPipe.fileHandleForReading
        outputHandle.waitForDataInBackgroundAndNotify()

        var observations = [NSObjectProtocol]()

        observations.append(NotificationCenter.default.addObserver(forName: NSNotification.Name.NSFileHandleDataAvailable, object: outputHandle, queue: nil) { _ in
            let outputData = outputHandle.availableData
            let outputString = String(data: outputData, encoding: .utf8)
            
            let lines = outputString?.components(separatedBy: "\n")
            var entries: [DataEntry] = []
            
            for line in lines ?? [] {
                let components = line.components(separatedBy: CharacterSet.whitespaces).filter { !$0.isEmpty }
                if components.count >= 8 {
                    let pid = components[0]
                    let command = components[1]
                    let cpu = components[2]
                    let threads = components[3]
                    let time = components[4]
                    let memory = components[5]
                    let state = components[6]
                    let user = components[7]
                    let priority = components[8]

                    if !pid.isEmpty, !threads.isEmpty, !cpu.isEmpty, !command.isEmpty, !time.isEmpty, !memory.isEmpty, !state.isEmpty, !user.isEmpty, !priority.isEmpty {
                        let entry = DataEntry(pid: pid, threads: threads, command: command, cpu: cpu, time: time, memory: memory, state: state, user: user, priority: priority)
                        entries.append(entry)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.exampleArray = entries
            }
            
            outputHandle.waitForDataInBackgroundAndNotify()
        })

        task.launch()
        
        cancellable = AnyCancellable {
            task.terminate()
            observations.forEach(NotificationCenter.default.removeObserver)
        }
    }
}

func processStateName(for code: String) -> String {
    switch code {
    case "S":
        return "Dormindo"
    case "Ss":
        return "Líder de Sessão"
    case "R":
        return "Executando"
    case "SX":
        return "Finalizado"
    case "Rs":
        return "Dormindo (Interrompível)"
    case "D":
        return "Dormindo (Ininterruptível)"
    case "Z":
        return "Zumbi"
    default:
        return code
    }
}

func performProcessAction(pid: Int32, action: String) {
    let task = Process()
    task.launchPath = "/bin/kill"
    
    switch action {
    case "kill":
        task.arguments = ["-9", String(pid)]
    case "stop":
        task.arguments = ["-STOP", String(pid)]
    case "continue":
        task.arguments = ["-CONT", String(pid)]
    default:
        return // Invalid action
    }
    
    task.launch()
    if (task.terminationStatus == 0){
        print(action + " successfully done")
    }
}

func changeProcessPriority(pid: Int32, priority: Int32) -> Bool {
    let task = Process()
    task.launchPath = "/usr/bin/renice"
    task.arguments = [String(priority), String(pid)]
    
    task.launch()
    task.waitUntilExit()
    
    return task.terminationStatus == 0
}

struct BottomView: View {
    @State private var pidText = ""
    @State private var priorityText = ""
    @State private var cpuText = ""
    
    var bindingFilterText: Binding<String> {
        Binding<String>(
            get: { filterText },
            set: { filterText = $0 }
        )
    }
    
    var body: some View {
        HStack {
            Spacer()
            TextField("Pid to process", text: $pidText)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.trailing, 8)
            Button(action: {
                if let pid = Int32(pidText){
                    performProcessAction(pid: pid, action:"kill")
                } else{
                    print("Error in kill pid")
                }
            }) {
                Text("Kill").frame(width:80)
            }
            .padding(.leading, 8)
            Spacer()
            Button(action: {
                if let pid = Int32(pidText) {
                    performProcessAction(pid: pid, action:"stop")
                } else {
                    print("Error in stop pid")
                }
            }) {
                Text("Stop").frame(width:80)
            }
            .padding(.leading, 8)
            Spacer()
            Button(action: {
                if let pid = Int32(pidText) {
                    performProcessAction(pid: pid,action: "continue")
                } else {
                    print("Error in continue pid")
                }
                
            }) {
                Text("Continue").frame(width:80)
            }
            .padding(.leading, 8)
            Spacer()
        }
        .padding(.horizontal)
        .padding(.bottom)
        

        HStack {
            HStack {
                Spacer()
                TextField("Priority", text: $priorityText)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.trailing, 8)
                
                Button(action: {
                    if let pid = Int32(pidText), let priority = Int32(priorityText){
                        changeProcessPriority(pid: pid, priority: priority)
                    } else{
                        print("Error in priority pid")
                    }
                }) {
                    Text("Confirm").frame(width:80)
                }
                .padding(.leading, 8)
                Spacer()
            }
            .padding(.horizontal)
            .padding(.vertical, 8)
            HStack {
                HStack {
                    Spacer()
                    TextField("CPU", text: $cpuText)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding(.trailing, 8)
                    Button(action: {
                        // Handle filter button action
                    }) {
                        Text("Confirm").frame(width:80)
                    }
                    .padding(.leading, 8)
                    Spacer()
                }
                .padding(.trailing)
                .padding(.vertical, 8)
            }
        }
        HStack {
            HStack {
                Spacer()
                TextField("Filter", text: bindingFilterText)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.trailing, 8)
            }
            .padding(.horizontal)
            .padding(.vertical, 8)
        }
    }
}

struct ContentView: View {
    @StateObject private var psCommandDataProvider = PSCommandDataProvider()
    
    var bindingFilterText: Binding<String> {
        Binding<String>(
            get: { filterText },
            set: { filterText = $0 }
        )
    }
    
    var filteredData: [DataEntry] {
        if filterText.isEmpty {
            return psCommandDataProvider.exampleArray
        } else {
            return psCommandDataProvider.exampleArray.filter { $0.pid.contains(filterText) }
        }
    }
        
    
    var body: some View {
        ZStack(alignment: .top) {
            GeometryReader { geometry in
                VStack(alignment: .leading) {
                    HStack {
                        Text("PID").frame(width:100,height:25, alignment:.center).bold()
                        Text("Name").frame(width:300,height:25, alignment:.center).bold()
                        Text("CPU (%)").frame(width:100,height:25, alignment:.center).bold()
                        Text("Threads").frame(width:100,height:25, alignment:.center).bold()
                        Text("Time").frame(width:100,height:25, alignment:.center).bold()
                        Text("Memory").frame(width:100,height:25, alignment:.center).bold()
                        Text("State").frame(width:100,height:25, alignment:.center).bold()
                        Text("User").frame(width:100,height:25, alignment:.center).bold()
                        Text("Priority").frame(width:100,height:25, alignment:.center).bold()
                    }
                    ScrollView {
                        VStack {
                            ForEach(filteredData) { dataEntry in
                                HStack {
                                    Text(dataEntry.pid).frame(width:100,height:25, alignment:.center)
                                    Text(dataEntry.command).frame(width:300,height:25, alignment:.center)
                                    Text(dataEntry.cpu).frame(width:100,height:25, alignment:.center)
                                    Text(dataEntry.threads).frame(width:100,height:25, alignment:.center)
                                    Text(dataEntry.time).frame(width:100,height:25, alignment:.center)
                                    Text(dataEntry.memory).frame(width:100,height:25, alignment:.center)
                                    Text(processStateName(for: dataEntry.state)).frame(width:100,height:25, alignment:.center)
                                    Text(dataEntry.user).frame(width:100,height:25, alignment:.center)
                                    Text(dataEntry.priority).frame(width:100,height:25, alignment:.center)
                                }
                            }
                        }
                    }
                    .alignmentGuide(.bottom) { _ in
                        geometry.size.height
                    }
                }
                .onAppear {
                    psCommandDataProvider.fetchData()
                }
            }
        }
    }
    

}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
//        ContentView()
        BottomView()
    }
}
